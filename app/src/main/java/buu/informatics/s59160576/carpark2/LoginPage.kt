package buu.informatics.s59160576.carpark2


import android.app.AlertDialog
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController


import buu.informatics.s59160576.carpark2.databinding.FragmentLoginPageBinding

/**
 * A simple [Fragment] subclass.
 */
class LoginPage : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding: FragmentLoginPageBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_login_page, container, false)

                binding.loginBtn.setOnClickListener{ View ->


            binding.apply {

                if(username.text.toString() == "ron" && password.text.toString() == "12345678"){

                    View.findNavController()
                        .navigate(R.id.action_loginPage2_to_carparkPage)

                }else{
                    val alertmsg = AlertDialog.Builder(context)
                    alertmsg.setMessage("You Username or Password is incorrect, Please Try again")
                    alertmsg.setCancelable(true)

                    alertmsg.setPositiveButton(
                        "ok"
                    ) { dialog, id -> dialog.cancel() }

                    val alert11 = alertmsg.create()
                    alert11.show()
                }
            }

        }

        return binding.root
    }


}
