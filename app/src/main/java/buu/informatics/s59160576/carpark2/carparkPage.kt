package buu.informatics.s59160576.carpark2


import android.content.Context
import android.databinding.DataBindingUtil
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import android.view.inputmethod.InputMethodManager
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI

import buu.informatics.s59160576.carpark2.databinding.FragmentCarparkPage2Binding


/**
 * A simple [Fragment] subclass.
 */
class carparkPage : Fragment() {
    private lateinit var binding: FragmentCarparkPage2Binding
    private val booking: ArrayList<slot_carpark> = ArrayList<slot_carpark>()
    private var slot:Int = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_carpark_page2,container,false)
        binding.park.setBackgroundResource(android.R.drawable.btn_default)
        binding.park2.setBackgroundResource(android.R.drawable.btn_default)
        binding.park3.setBackgroundResource(android.R.drawable.btn_default)

        for(x in 0..2){
            booking.add(x, slot_carpark("","",""))
        }
        binding.apply {
            park.setOnClickListener{
                slot = 0
                showValue(slot)
            }
            park2.setOnClickListener{
                slot = 1
                showValue(slot)
            }
            park3.setOnClickListener{
                slot = 2
                showValue(slot)
            }
            submit.setOnClickListener{
                setValueBooking(it)
            }
            delete.setOnClickListener{
                setVisibleIsTrue()
            }
        }
        setHasOptionsMenu(true)
        return binding.root
    }
    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.about, menu)
    }
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return NavigationUI.onNavDestinationSelected(item!!,
            view!!.findNavController())
                || super.onOptionsItemSelected(item)
    }
    private fun setVisibleIsTrue() {
        binding.registration.setText("")
        binding.band2.setText("")
        binding.name.setText("")

        if (slot == 0) {
            binding.park.setText("EMPTY")
            binding.park.setBackgroundResource(android.R.drawable.btn_default)
            booking.removeAt(0)
            booking.add(0, slot_carpark("","",""));
        } else if (slot == 1) {
            binding.park2.setText("EMPTY")
            binding.park2.setBackgroundResource(android.R.drawable.btn_default)
            booking.removeAt(1)
            booking.add(1, slot_carpark("","",""));

        } else if (slot == 2) {
            binding.park3.setText("EMPTY")
            binding.park3.setBackgroundResource(android.R.drawable.btn_default)
            booking.removeAt(2)
            booking.add(2, slot_carpark("","",""));
        }
    }



    private fun setValueBooking(view: View) {
        val registrationnumber = binding.registration.text.toString()
        val brand = binding.band2.text.toString()
        val name = binding.name.text.toString()
        booking.add(slot , slot_carpark(registrationnumber, brand, name))
        if (slot == 0) {
            binding.park.setBackgroundColor(Color.RED)
            binding.park.setText("FULL")
            binding.park.setTextColor(Color.WHITE)


        } else if (slot == 1) {
            binding.park2.setText("FULL")
            binding.park2.setBackgroundColor(Color.RED)
            binding.park2.setTextColor(Color.WHITE)
        } else {
            binding.park3.setText("FULL")
            binding.park3.setBackgroundColor(Color.RED)
            binding.park3.setTextColor(Color.WHITE)
        }

        val context = this.getContext()
        val inputMethodManager =
            context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)

    }



    private fun showValue(slot: Int) {
        binding.registration.setText(booking.get(slot).Registration)
        binding.band2.setText(booking.get(slot).band)
        binding.name.setText(booking.get(slot).name)
    }



}
